import RESTSerializer from 'ember-data/serializers/rest';

export default RESTSerializer.extend({
  normalizeResponse(store, modelClass, payload, id, requestType) {
    payload.github_id = payload.id;
    payload.id = id;

    let newPayload = {
      [modelClass.modelName]: payload
    };

    return this._super(store, modelClass, newPayload, id, requestType);
  },

  normalize(modelClass, responseHash, prop) {
    let hash = {};

    Object.keys(responseHash).forEach(key => {
      let value = responseHash[key];
      hash[key.camelize()] = value;
    });

    // console.log(Object.keys(hash).filter(key => !/_url$/.test(key)).map(key => key.camelize()).join("\n"));

    return this._super(modelClass, hash, prop);
  }
});
