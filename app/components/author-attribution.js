import Ember from 'ember';
import moment from 'moment';

export default Ember.Component.extend({
  tagName: 'div',

  classNames: ['issue-comment-attribution'],

  edited: Ember.computed(function() {
    return moment(this.get('comment.updatedAt')).isAfter(this.get('comment.createdAt'));
  })
});
